%{
    import coolc.ast.*;
/*Prologue */
%}

/*bison declaration*/
%language "Java"

%define package coolc.parser;
%define parser_class_name "Parser"
%define public

%code {
    private Program _root;
    public Program getRoot() {
        return _root;
    }

    public static String getTokenName(int t) {
        return yytname_[t-255];
    }

}

/*x op y op z
* %left specifies left-associativity (grouping x with y first)
* %right specifies right-associativity (grouping y with z first)
* %nonassoc specifies no associativity, which means that ‘x op y op z’ is considered a syntax error
*/
%right "<-"
%nonassoc "not"
%left "<=" "<" "="
%left "+" "-" 
%left "*" "/"
%nonassoc "isvoid"
%nonassoc "~"
%nonassoc "@"
%nonassoc "."

%locations

%token  T_LPAREN "(" T_RPAREN ")"
%token  T_LCURLY "{" T_RCURLY "}"
%token  T_COLON ":" T_SEMICOLON ";"
%token  T_DOT "." T_COMMA ","

%token T_CLASS      "class"
%token T_ELSE       "else"
%token T_FI         "fi"
%token T_IF         "if"
%token T_IN         "in"
%token T_INHERITS   "inherits"
%token T_ISVOID     "isvoid"
%token T_LET        "let"
%token T_LOOP       "loop"
%token T_POOL       "pool"
%token T_THEN       "then"
%token T_WHILE      "while"
%token T_CASE       "case"
%token T_ESAC       "esac"
%token T_NEW        "new"
%token T_OF         "of"
%token T_NOT        "not"

%token <Boolean> T_TRUE       "true"
%token <Boolean>T_FALSE      "false"

%token T_CAST "@"
%token T_NEGATE "~"
%token T_MULT "*" T_DIV "/"
%token T_PLUS "+" T_MINUS "-"
%token T_LTE "<=" T_LT "<" T_EQUALS "="
%token T_ASSIGN "<-"
%token T_FAT "=>"

%token T_UNKNOWN

%token <ValueNode> BOOL
%token <ValueNode> TYPE
%token <ValueNode> ID
%token <ValueNode> STRING
%token <ValueNode> INTEGER

/* FALTA ANALISAR O QUE ESSES SAO NO MEU CASO  SHEILA*/
%define stype AstNode

%type <AstNode> program class_list class_decl feature_list feature formal_list expr idtypeexpr formal_list_not_empty formal expr_list expr_blocks idtype_list idtype2_list expr_list_not_empty

%%
/*Grammar rules */
program
	: class_list 				{ root = $1;}
	;

class_list
	: class_decl 				{ $$ = new AstNode("program", $class_decl);}
	| class_list class_decl 		{$1.children.add($2); $$ = $1;}
	;

class_decl
	: "class" TYPE "{" feature_list "}" 	{$$ = new ClassNode((String)$TYPE.getValue(), $feature_list);}
	| "class" TYPE "inherits" TYPE "{" feature_list "}" {$$ = new ClassNode((String)$2.getValue(), (String)$4.getValue(), $feature_list);}
	;

/* falta fazer o retorno das variáveis */

feature_list[listf]
	: /* empty */				{$$ = null;}
	| feature_list feature ";" 		{$listf.children.add($feature); $listf = $1;}
	;

feature
	: ID "(" formal_list ")" ":" TYPE "{" expr "}" {$$ = new MethodNode((String)$ID.getValue(), $formal_list, (String)$TYPE.getValue(), $expr);}
	| ID ":" TYPE				
	| ID ":" TYPE "<-" expr			
	; 

/* [formal[,formal]*] */

formal_list
	: /*empty*/ 				{$$ = null;}
	| formal_list_not_empty			{$$ = $1;}  
	;

formal_list_not_empty
	: formal
	| formal_list_not_empty "," formal 
	;

formal
	: ID ":" TYPE
	; 

expr
	: ID "<-" expr 
	| expr "." ID "(" expr_list ")" 
	| expr "@" TYPE "." ID "(" expr_list ")" 
	| ID "(" expr_list ")" 
	| "if" expr "then" expr "else" expr "fi"
	| "while" expr "loop" expr "pool"
	| "{" expr_blocks "}"
	| "let" idtypeexpr idtype_list "in" expr
	| "case" expr "of" idtype2_list "esac"
	| "new" TYPE
	| "isvoid" expr
	| expr "+" expr
	| expr "-" expr
	| expr "*" expr
	| expr "/" expr
	| expr "~"
	| expr "<" expr
	| expr "<=" expr
	| expr "=" expr
	| "not" expr
	| "(" expr ")"
	| ID			{$$ = $1;}
	| INTEGER		{$$ = $1;}
	| STRING		{$$ = $1;}
	| "true"
	| "false"
	| BOOL			{$$ = $1;}
	;
	
/* expr[,expr]* */
expr_list
	: /* empty */		{$$ = null;}
	| expr_list_not_empty
	;                             

expr_list_not_empty
	: expr
	| expr_list_not_empty "," expr
	;

/* [expr;]+ */
expr_blocks
	: expr ";"
	| expr_blocks expr ";"
	;

/* ID : TYPE [<- expr ] */
idtypeexpr
	: ID ":" TYPE
	| ID ":" TYPE "<-" expr
	;

idtype_list
	: /*empty */	{$$ = null;}
	| idtype_list "," idtypeexpr
	;

idtype2_list
	: ID ":" TYPE "=>" expr ";"
	| idtype2_list ID ":" TYPE "=>" expr ";"
	;
%%
/*Epilogue*/


