package coolc;

import coolc.parser.*;
import coolc.ast.*;

import java.io.*;
import java.util.*;

public class Coolc {


    public static void printInstructions() {
        System.out.println(
            "Usage Coolc <action> file1 file2 ... filen\n" +
            "Available actions: \n" +
            "scan - scans each files and outputs a token list\n" +
            "parse - parses each file and outputs the syntax tree\n"
            );
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {

        if (args.length < 2) {
            printInstructions();
            return;
        }


        String action = args[0];

        List<String> files = Arrays.asList(args).subList(1, args.length);

        switch(action) {

            case "scan":
                scan(files);
                break;

            case "parse":
                parse(files);
                break;

            default:
                printInstructions();
                return;
        }


    }

    private static void scan(List<String> files) throws FileNotFoundException, IOException {

        for(String filename: files) {
            File f = new File(filename);
            Reader r = new FileReader(f);
            Lexer lex = new Lexer(r);
            Random rand = new Random();

            for(int token = lex.yylex(); token != Parser.EOF; token = lex.yylex()) {

                Position pos = lex.getStartPos();

                switch(token) {

                    case Parser.ID:
                        System.out.printf("%s:%2d:%2d  Id<%s>\n", f.getPath(), pos.line, pos.column, lex.getLVal());
                        break;

                    case Parser.INTEGER:
                        System.out.printf("%s:%2d:%2d  Int<%s>\n", f.getPath(), pos.line, pos.column, lex.getLVal());
                        break;
                        

                    case Parser.BOOL:
                        System.out.printf("%s:%2d:%2d  Boolean<%s>\n", f.getPath(), pos.line, pos.column, (Boolean)lex.getLVal() ? "True" : "False");
                        break;
                        
                    case Parser.TYPE:
                        System.out.printf("%s:%2d:%2d  Type<%s>\n", f.getPath(), pos.line, pos.column, lex.getLVal());
                        break;

                    case Parser.STRING:
                        
                        String strval =((String)lex.getLVal()).replace("\n","").replace("\b", "").replace("\t", "").replace("\f", "").replace(">", "");
                        
                        strval = strval.substring(0, Math.min(strval.length(), 20));


                        System.out.printf("%s:%2d:%2d  String<%s>\n", f.getPath(), pos.line, pos.column, strval);
                        break;

                    default:
                        System.out.printf("%s:%2d:%2d  %s\n", f.getPath(), pos.line, pos.column, Parser.getTokenName(token));
                        break;
                }

            }
        }
    }

/* COISA NOVA */

    private static void parse(List<String> files) throws FileNotFoundException, IOException { 
        
        for(String filename: files) {        
            File f = new File(filename);
            Reader r = new FileReader(f);
            Lexer lex = new Lexer(r);
            Parser p = new Parser(lex);
            // p.setDebugLevel(1);

            p.parse();
            if( p.getRoot() != null) {
                print(p.getRoot(), 0);
            }
        }
    }


    private static void print(AstNode n, int indent) {

    	private StringBuilder strmethod = new StringBuilder();
	/* Cada linha passa por aqui  SHEILA*/
	
	/* Aqui faz a identacao SHEILA*/
        for(int i = indent; i > 0; i--) {
            System.out.print("    ");
        }

        if(n instanceof ValueNode) {
            //System.out.printf("valuenode \n");	
            System.out.printf("%s %s\n", n.tag, ((ValueNode)n).getValue());
        }
        else if(n instanceof CallNode) {
            //System.out.printf("callnode \n");
            CallNode call = (CallNode)n;
            System.out.println("call " + call.getId());

            /*Vendo os filhos*/
            if(call.getArgs() != null )
		/*Aqui esta a recursão*/
                for(AstNode c: call.getArgs().children) {
                    if(c != null) {
                        print(c, indent+1);
                    }
                }
        }

        else if(n instanceof ClassNode) {

            ClassNode clase = (ClassNode)n;

            if (clase.getIdPai() != null) {
            	System.out.println("class " + clase.getId() + " : " + clase.getIdPai());
            }

            else 	
            	System.out.println("class " + clase.getId());

            /*Vendo os filhos*/
            for(AstNode c: clase.children) {
                if(c != null) {
                    print(c, indent+1);
                }
            }
        }

        else if(n instanceof MethodNode) {

            strmethod.setLength(0);
	
            MethodNode method = (MethodNode)n;

            strmethod.append("method ");
            strmethod.append(method.getId());
            strmethod.append(" : ");
            /* Por enquanto sem o formal_list*/
            strmethod.append(method.getType());		
            System.out.println(strmethod);

            /*Vendo os filhos*/
            for(AstNode c: method.children) {
                if(c != null) {
                    print(c, indent+1);
                }
            }
        }
	
        else {
            //System.out.printf("nao valuenode nao callnode\n");		
            System.out.println(n.tag);

            for(AstNode c: n.children) {
                if(c != null) {
                    print(c, indent+1);
                }
            }
        }
    }


}  
