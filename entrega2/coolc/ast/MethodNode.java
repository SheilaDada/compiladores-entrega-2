package coolc.ast;

public class MethodNode extends AstNode {

    private AstNode args;
    public AstNode getArgs() {
        return args;
    }

    private String id;
    public String getId() {
        return id;
    }

    private String type;
    public String getType() {
        return type;
    }

    public MethodNode(String id, AstNode args, String type, AstNode params) {
        super("method", params);
        this.id = id;
        this.type = type;
        this.args = args;
    }

}
