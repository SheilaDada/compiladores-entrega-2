%{
    package coolc.parser;	
    import coolc.ast.*;
/*Prologue */
%}

/*bison declaration*/
%output "coolc/parser/Parser.java"
%language "Java"

//%define package coolc.parser
%define parser_class_name "Parser"
%define public

%code {
    private AstNode root;
    public AstNode getRoot() {
        return root;
    }


    public static String getTokenName(int t) {
        return yytname_[t-255];
    }

}

/*x op y op z
* %left specifies left-associativity (grouping x with y first)
* %right specifies right-associativity (grouping y with z first)
* %nonassoc specifies no associativity, which means that ‘x op y op z’ is considered a syntax error
*/
%right "<-"
%nonassoc "not"
%left "<=" "<" "="
%left "+" "-" 
%left "*" "/"
%nonassoc "isvoid"
%nonassoc "~"
%nonassoc "@"
%nonassoc "."

%locations

%token  T_LPAREN "(" T_RPAREN ")"
%token  T_LCURLY "{" T_RCURLY "}"
%token  T_COLON ":" T_SEMICOLON ";"
%token  T_DOT "." T_COMMA ","

%token T_CLASS      "class"
%token T_ELSE       "else"
%token T_FI         "fi"
%token T_IF         "if"
%token T_IN         "in"
%token T_INHERITS   "inherits"
%token T_ISVOID     "isvoid"
%token T_LET        "let"
%token T_LOOP       "loop"
%token T_POOL       "pool"
%token T_THEN       "then"
%token T_WHILE      "while"
%token T_CASE       "case"
%token T_ESAC       "esac"
%token T_NEW        "new"
%token T_OF         "of"
%token T_NOT        "not"

//%token <Boolean> T_TRUE       "true"
//%token <Boolean>T_FALSE      "false"

%token T_CAST "@"
%token T_NEGATE "~"
%token T_MULT "*" T_DIV "/"
%token T_PLUS "+" T_MINUS "-"
%token T_LTE "<=" T_LT "<" T_EQUALS "="
%token T_ASSIGN "<-"
%token T_FAT "=>"

%token T_UNKNOWN

%token <ValueNode> BOOL
%token <ValueNode> TYPE
%token <ValueNode> ID
%token <ValueNode> STRING
%token <ValueNode> INTEGER

/* FALTA ANALISAR O QUE ESSES SAO NO MEU CASO  SHEILA*/
%define stype AstNode

%type <AstNode> program class_list class_decl feature_list feature formal_list expr idtypeexpr formal_list_not_empty formal expr_list expr_blocks idtype_list idtype2_list expr_list_not_empty feature_list_not_empty

%%
/*Grammar rules */
program
	: class_list 				{ root = $1;}
	;

class_list
	: class_decl ";" 				{$$ = new AstNode("program", $class_decl);}
	| class_list class_decl ";"		{$1.children.add($2); $$ = $1;}
	;

class_decl
	: "class" TYPE "{" feature_list "}" 	{$$ = new ClassNode((String)$TYPE.getValue(), $feature_list);}
	| "class" TYPE "inherits" TYPE "{" feature_list "}" {$$ = new ClassNode((String)$2.getValue(), (String)$4.getValue(), $feature_list);}
	;

/* falta fazer o retorno das variáveis */

feature_list
	: /* empty */				{$$ = null;}
	| feature_list_not_empty 		{$$ = $1;}
	;

feature_list_not_empty
	: feature ";"				{ $$ = new AstNode("feature", $feature); }
	| feature_list_not_empty feature ";"	{ $1.children.add($feature); $$ = $1; }  		
	;

feature
	: ID "(" formal_list ")" ":" TYPE "{" expr "}" {$$ = new MethodNode((String)$ID.getValue(), $formal_list, (String)$TYPE.getValue(), $expr);}
	| ID ":" TYPE				{$$ = new FieldNode((String)$ID.getValue(), (String)$TYPE.getValue());} 		
	| ID ":" TYPE "<-" expr			{$$ = new FieldNode((String)$ID.getValue(), (String)$TYPE.getValue(), $expr);} 		
	;

/* [formal[,formal]*] */

formal_list
	: /*empty*/ 				{$$ = null;}
	| formal_list_not_empty			{$$ = $1;}  
	;

formal_list_not_empty
	: formal				{$$ = new AstNode("formal", $formal);}			
	| formal_list_not_empty "," formal 	{$1.children.add($formal); $$ = $1;}  
	;

formal
	: ID ":" TYPE				{$$ = new FormalNode((String)$ID.getValue(), (String)$TYPE.getValue());}
	;

expr[exprTotal]
	: ID "<-" expr  			{$$ = new AssignNode((String)$ID.getValue(), $3);}  
	| expr "." ID "(" expr_list ")"		{AstNode expr2 = new AstNode("callee", $1); $$ = new CalleNode(expr2,(String)$ID.getValue(), $5 );}
	| expr "@" TYPE "." ID "(" expr_list ")" {AstNode expr2 = new AstNode("callee", $1); $$ = new CalleNode(expr2,(String)$ID.getValue(), $7,(String)$TYPE.getValue() );}
	| ID "(" expr_list ")" 			{$$ = new CallNode("call", (String)$ID.getValue(), $3); }  
	| "if" expr "then" expr "else" expr "fi" {$$ = new IfNode(new AstNode("if",$2),new AstNode("then",$4), new AstNode("else",$6) ); }  
	| "while" expr "loop" expr "pool" 	{$$ = new WhileNode(new AstNode("while", $2), new AstNode("loop",$4));}  
	| "{" expr_blocks "}" 			{$$ = $2;}  
	| "let" idtype_list "in" expr 		{$$ = new AstNode("let", $2, $4);}  
	| "case" expr "of" idtype2_list "esac" 	{$$ = new CaseNode($2, $4);}  
	| "new" TYPE 				{$$ = new ValueNode("new", (String)$TYPE.getValue());}  
	| "isvoid" expr 			{$$ = new CallNode("unary", "isvoid", $2);}  
	| expr "+" expr 			{$$ = new CallNode("binary", "+", $1, $3);}    
	| expr "-" expr 			{$$ = new CallNode("binary", "-", $1, $3);}    
	| expr "*" expr 			{$$ = new CallNode("binary", "*", $1, $3);}   
	| expr "/" expr				{$$ = new CallNode("binary", "/", $1, $3);}   
	| "~" expr 				{$$ = new CallNode("unary", "~", $2);}   
	| expr "<" expr 			{$$ = new CallNode("binary", "<", $1, $3);}    
	| expr "<=" expr 			{$$ = new CallNode("binary", "<=", $1, $3);}   
	| expr "=" expr 			{$$ = new CallNode("binary", "=", $1, $3);}    
	| "not" expr 				{$$ = new CallNode("unary", "not", $2);}   
	| "(" expr ")" 				{$$ = $2;}  
	| ID					{$$ = $1;}
	| INTEGER				{$$ = $1;}
	| STRING				{$$ = $1;}
	| "true" 				{$$ = new ValueNode("bool", "true");}  
	| "false"				{$$ = new ValueNode("bool", "false");}  
	| BOOL					{$$ = $1;}
	;
	
/* expr[,expr]* */
expr_list
	: /* empty */				{$$ = null;}
	| expr_list_not_empty 			{$$ = $1;}  
	;                             

expr_list_not_empty
	: expr 					{$$ = new AstNode("args", $expr);}  
	| expr_list_not_empty "," expr 		{$1.children.add($3); $$ = $1;}  
	;

/* [expr;]+ */
expr_blocks
	: expr ";" 				{$$ = new AstNode("block", $expr);}  
	| expr_blocks expr ";" 			{$1.children.add($2); $$ = $1;}  
	;

/* ID : TYPE [<- expr ] */
idtypeexpr
	: ID ":" TYPE 				{$$ = new IdTypeNode((String)$ID.getValue(), (String)$TYPE.getValue());}  
	| ID ":" TYPE "<-" expr 		{$$ = new IdTypeNode((String)$ID.getValue(), (String)$TYPE.getValue(), $expr);}   
	;

idtype_list
	: idtypeexpr 				{$$ = new AstNode("vars", $idtypeexpr);}
	| idtype_list "," idtypeexpr 		{$1.children.add($3); $$ = $1;}  
	;

idtype2_list
	: ID ":" TYPE "=>" expr ";" 		{$$ = new AstNode("idtype2list", new IdType2Node((String)$ID.getValue(), (String)$TYPE.getValue(), $5));}  
	| idtype2_list ID ":" TYPE "=>" expr ";" {$1.children.add(new IdType2Node((String)$ID.getValue(), (String)$TYPE.getValue(), $6)); $$ = $1;}  
	;
%%
/*Epilogue*/


