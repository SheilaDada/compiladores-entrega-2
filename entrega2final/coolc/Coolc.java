package coolc;

import coolc.parser.*;
import coolc.ast.*;

import java.io.*;
import java.util.*;

public class Coolc {


    public static void printInstructions() {
        System.out.println(
            "Usage Coolc <action> file1 file2 ... filen\n" +
            "Available actions: \n" +
            "scan - scans each files and outputs a token list\n" +
            "parse - parses each file and outputs the syntax tree\n"
            );
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {

        if (args.length < 2) {
            printInstructions();
            return;
        }


        String action = args[0];

        List<String> files = Arrays.asList(args).subList(1, args.length);

        switch(action) {

            case "scan":
                scan(files);
                break;

            case "parse":
                parse(files);
                break;

            default:
                printInstructions();
                return;
        }


    }

    private static void scan(List<String> files) throws FileNotFoundException, IOException {

        for(String filename: files) {
            File f = new File(filename);
            Reader r = new FileReader(f);
            Lexer lex = new Lexer(r);
            Random rand = new Random();

            for(int token = lex.yylex(); token != Parser.EOF; token = lex.yylex()) {

                Position pos = lex.getStartPos();

                switch(token) {

                    case Parser.ID:
                        System.out.printf("%s:%2d:%2d  Id<%s>\n", f.getPath(), pos.line, pos.column, (String)lex.getLVal().getValue());
                        break;

                    case Parser.INTEGER:
                        System.out.printf("%s:%2d:%2d  Int<%s>\n", f.getPath(), pos.line, pos.column, (String)lex.getLVal().getValue());
                        break;
                        

                    case Parser.BOOL:
                        System.out.printf("%s:%2d:%2d  Boolean<%s>\n", f.getPath(), pos.line, pos.column, (String)lex.getLVal().getValue());
                        break;
                        
                    case Parser.TYPE:
                        System.out.printf("%s:%2d:%2d  Type<%s>\n", f.getPath(), pos.line, pos.column, (String)lex.getLVal().getValue());
                        break;

                    case Parser.STRING:
                        
                        String strval =((String)lex.getLVal().getValue()).replace("\n","").replace("\b", "").replace("\t", "").replace("\f", "").replace(">", "");
                        
                        strval = strval.substring(0, Math.min(strval.length(), 20));


                        System.out.printf("%s:%2d:%2d  String<%s>\n", f.getPath(), pos.line, pos.column, strval);
                        break;

                    default:
                        System.out.printf("%s:%2d:%2d  %s\n", f.getPath(), pos.line, pos.column, Parser.getTokenName(token));
                        break;
                }

            }
        }
    }

/* COISA NOVA */

    private static void parse(List<String> files) throws FileNotFoundException, IOException { 
        
        for(String filename: files) {        
            File f = new File(filename);
            Reader r = new FileReader(f);
            Lexer lex = new Lexer(r);
            Parser p = new Parser(lex);
            // p.setDebugLevel(1);

            p.parse();
            if( p.getRoot() != null) {
                print(p.getRoot(), 0);
            }
        }
    }


    private static void print(AstNode n, int indent) {

    	StringBuilder strmethod = new StringBuilder();
	/* Cada linha passa por aqui  SHEILA*/
	
	/* Aqui faz a identacao SHEILA*/
	if (!(n instanceof IfNode) && !(n instanceof WhileNode) ){
		for(int i = indent; i > 0; i--) {
			System.out.print("    ");
		}
	/****** VALUENODE */
		if(n instanceof ValueNode) {
		    if (!(n.tag.equals("str"))){
			    System.out.printf("%s %s\n", n.tag, ((ValueNode)n).getValue());
		    }
		    else{
			StringBuilder stringBuilder = new StringBuilder((String)((ValueNode)n).getValue());  
			stringBuilder.insert(0, '"');
			stringBuilder.append('"');
			String strstr = stringBuilder.toString();
			strstr = strstr.replace("\n","\\n").replace("\b", "\\b").replace("\t", "\\t").replace("\f", "\\f"); 
			System.out.printf("%s %s\n", n.tag , strstr);  

		    }
		}
 
	/****** CALLNODE Y UNARYCODE */
		else if(n instanceof CallNode) {
		    //System.out.printf("callnode \n");
		    CallNode call = (CallNode)n;
		    System.out.println(call.tag + " " + call.getId());

		    /*Vendo os filhos, aqui eh a tecnica que ele pula e nao precisa imprimir*/
		    for(AstNode c: call.children) {
		        if(c != null) {
		             print(c, indent+1);
		        }
		    }
		}

	/****** CLASSNODE */
		else if(n instanceof ClassNode) {

		    ClassNode clase = (ClassNode)n;

		    if (clase.getIdPai() != null) {
		    	System.out.println("class " + clase.getId() + " : " + clase.getIdPai());
		    }

		    else 	
		    	System.out.println("class " + clase.getId());

		    /*Vendo os filhos*/
		    if(clase.getArgs() != null )
			/*Aqui esta a recursão*/
		        for(AstNode c: clase.getArgs().children) {
		            if(c != null) {
		                print(c, indent+1);
		            }
		        }
		}


	/****** CASENODE */
		else if(n instanceof CaseNode) {

		    CaseNode caso = (CaseNode)n;

		    System.out.println(caso.tag);

		    for(AstNode c: caso.children) {
		        if(c != null) {
		            print(c, indent+1);
		        }
		    }

		    /*Vendo os filhos*/
		    if(caso.getArgs() != null )
			/*Aqui esta a recursão*/
		        for(AstNode d: caso.getArgs().children) {
		            if(d != null) {
		                print(d, indent+1);
		            }
		        }
		}

	/****** METHODNODE */
		else if(n instanceof MethodNode) {

		    strmethod.setLength(0);
	
		    MethodNode method = (MethodNode)n;

		    strmethod.append("method ");
		    strmethod.append(method.getId());
		    strmethod.append(" : ");
		    /* PODE DAR PROBLEMA AQUI */
		    if(method.getArgs() != null ){
			    for (AstNode d: method.getArgs().children){
				if (d != null){
					if (d instanceof FormalNode){
						FormalNode formal = (FormalNode) d;
						strmethod.append(formal.getType());
						strmethod.append(" ");
						strmethod.append(formal.tag);
						strmethod.append(" -> ");
					}
				}
			    }
		    }		
		    /* Por enquanto sem o formal_list*/
		    strmethod.append(method.getType());		
		    System.out.println(strmethod.toString());

		    /*Vendo os filhos*/
		    for(AstNode c: method.children) {
		        if(c != null) {
		            print(c, indent+1);
		        }
		    }
		}

	/****** FIELDNODE */
		else if(n instanceof FieldNode) {

		    FieldNode field = (FieldNode)n;
		    
		    System.out.println("field " + field.getType() + " " + field.getId());

		    /*Vendo os filhos*/
		    for(AstNode c: field.children) {
		        if(c != null) {
		            print(c, indent+1);
		        }
		    }
		}

	/****** IDTYPENODE */
		else if(n instanceof IdTypeNode) {

		    IdTypeNode idtype = (IdTypeNode)n;
		    
		    System.out.println(idtype.getType() + " " + idtype.getId());

		    /*Vendo os filhos*/
		    for(AstNode c: idtype.children) {
		        if(c != null) {
		            print(c, indent+1);
		        }
		    }
		}

	/****** IDTYPENODE2 */
		else if(n instanceof IdType2Node) {

		    IdType2Node idtype2 = (IdType2Node)n;
		    
		    System.out.println(idtype2.tag + " " + idtype2.getType() + " " + idtype2.getId());

		    /*Vendo os filhos*/
		    for(AstNode c: idtype2.children) {
		        if(c != null) {
		            print(c, indent+1);
		        }
		    }
		}

	/****** ASSIGNNODE*/
		else if(n instanceof AssignNode) {

		    AssignNode assign = (AssignNode)n;
		    
		    System.out.println("assign " + assign.getId() );

		    /*Vendo os filhos*/
		    for(AstNode c: assign.children) {
		        if(c != null) {
		            print(c, indent+1);
		        }
		    }
		}

	/****** CALLENODE */
		else if(n instanceof CalleNode) {

		    CalleNode calle = (CalleNode)n;

		    if (calle.getType() != null) {
		    	System.out.println("call " + calle.getId() + " as " + calle.getType());
		    }

		    else 	
		    	System.out.println("call " + calle.getId());

		    for(AstNode c: calle.children) {
		            if(c != null) {
		                print(c, indent+1);
		            }
		        }
		}

	/****** ASTNODE */	
		else {
		    /*if (n.tag.equals("program")){		
		    	for(AstNode c: n.children) {
				System.out.println("AQUIII" + c.tag);
		        }
		    }*/
		   		
			    System.out.println(n.tag);

			    for(AstNode c: n.children) {
				if(c != null) {
				    print(c, indent+1);
				}
			    }
		   
		}

	}

	else{
		//if(n instanceof IfNode){
		for(AstNode c: n.children) {
			if(c != null) {
				print(c, indent);
			}
		}
		//}	
	}

    }


}  
