package coolc.ast;

public class AssignNode extends AstNode {


    private String id;
    public String getId() {
        return id;
    }

    public AssignNode(String id, AstNode params) {
        super("assign", params);
        this.id = id;
    }

}
