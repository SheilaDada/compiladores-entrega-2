package coolc.ast;

public class CallNode extends AstNode {

    private String id;
    public String getId() {
        return id;
    }

    public CallNode(String tag ,String id, AstNode params) {
        super(tag, params);
        this.id = id;
    }

    public CallNode(String tag ,String id, AstNode params, AstNode params1) {
        super(tag, params, params1);
        this.id = id;
    }

}
