package coolc.ast;

public class CalleNode extends AstNode {


    private String id;
    public String getId() {
        return id;
    }

    private String type = null;
    public String getType() {
	if (type != null)        
		return type;
	else
		return null;
    }

    public CalleNode(AstNode expr2, String id, AstNode params) {
		super("calle", expr2, params);
		this.id = id;

    }

    public CalleNode(AstNode expr2, String id, AstNode params, String type) {

		super("calle", expr2, params);
		this.id = id;
		this.type = type;
    }

}
