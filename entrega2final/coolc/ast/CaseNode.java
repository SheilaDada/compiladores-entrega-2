package coolc.ast;

public class CaseNode extends AstNode {



    private AstNode args;
    public AstNode getArgs() {
        return args;
    }

    public CaseNode(AstNode params, AstNode args) {
        super("instanceof", params);
        this.args = args;
    }

}
