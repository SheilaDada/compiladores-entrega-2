package coolc.ast;

public class ClassNode extends AstNode {


    private String id;
    public String getId() {
        return id;
    }

    private String idPai = null;
    public String getIdPai() {
	if (idPai != null)        
		return idPai;
	else
		return null;
    }

    private AstNode args;
    public AstNode getArgs() {
        return args;
    }

    public ClassNode(String id, AstNode args) {
        super("class");
        this.id = id;
        this.args = args;
    }

    public ClassNode(String id, String idPai, AstNode args) {
        super("class");
        this.id = id;
        this.idPai = idPai;
        this.args = args;
    }

}
