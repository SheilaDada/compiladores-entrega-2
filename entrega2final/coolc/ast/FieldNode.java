package coolc.ast;

public class FieldNode extends AstNode {


    private String id;
    public String getId() {
        return id;
    }

    private String type;
    public String getType() {
        return type;
    }

    public FieldNode(String id, String type) {
        super("field");
        this.id = id;
	this.type = type;
    }

    public FieldNode(String id, String type, AstNode params) {
        super("field", params);
        this.id = id;
        this.type = type;
    }

}
