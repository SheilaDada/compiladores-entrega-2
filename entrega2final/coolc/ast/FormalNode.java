package coolc.ast;


public class FormalNode extends AstNode {


    private String type;
    public String getType() {
        return type;
    }

    public FormalNode(String id, String type) {
        super(id);
        this.type = type;
    }    

}
