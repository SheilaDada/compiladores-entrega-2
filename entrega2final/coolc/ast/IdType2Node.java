package coolc.ast;

public class IdType2Node extends AstNode {


    private String id;
    public String getId() {
        return id;
    }

    private String type;
    public String getType() {
        return type;
    }


    public IdType2Node(String id, String type, AstNode params) {
        super("case", params);
        this.id = id;
        this.type = type;
    }

}
