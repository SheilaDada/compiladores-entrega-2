package coolc.ast;

public class IdTypeNode extends AstNode {


    private String id;
    public String getId() {
        return id;
    }

    private String type;
    public String getType() {
        return type;
    }

    public IdTypeNode(String id, String type) {
        super("idtype");
        this.id = id;
	this.type = type;
    }

    public IdTypeNode(String id, String type, AstNode params) {
        super("idtype", params);
        this.id = id;
        this.type = type;
    }

}
