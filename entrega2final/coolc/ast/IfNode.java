package coolc.ast;

public class IfNode extends AstNode {

    public IfNode(AstNode ifExpr, AstNode thenExpr, AstNode elseExpr) {
        super("Ifexpr", ifExpr, thenExpr, elseExpr);
    }

}
