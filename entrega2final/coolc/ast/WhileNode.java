package coolc.ast;

public class WhileNode extends AstNode {

    public WhileNode(AstNode whileExpr, AstNode loopExpr) {
        super("WhileExpr", whileExpr, loopExpr);
    }

}
